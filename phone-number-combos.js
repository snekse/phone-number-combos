var PhoneNumberControls = PhoneNumberControls || {};

(function (ns) {

    // An numeric index to possible letters mapping using the array index as the key
    var numberMap = [
        /* 0 */ [],
        /* 1 */ [],
        /* 2 */ ["A", "B", "C"],
        /* 3 */ ["D", "E", "F"],
        /* 4 */ ["G", "H", "I"],
        /* 5 */ ["J", "K", "L"],
        /* 6 */ ["M", "N", "O"],
        /* 7 */ ["P", "Q", "R", "S"],
        /* 8 */ ["T", "U", "V"],
        /* 9 */ ["W", "X", "Y", "Z"]
    ];


    var buildCombos = function (n) {
        console.log('Building combos for ' + n);
        var possibilities = buildPossibilities(n);

        var combos = [];
        recurse(combos, possibilities, [], 0);
        return combos;
    };


    /**
     *  Given a number of 2127, `possibilities` ends up being
     *  [ ["A", "B", "C"], ["A", "B", "C"], ["P", "Q", "R", "S"] ]
     *  NOTE: The empty arrays for 0 & 1 have been removed
     */
    var buildPossibilities = function (n) {
        var possibilities = ('' + n).split('').reduce((acc, strDigit) => {
            var charArray = numberMap[parseInt(strDigit, 10)];
            if (charArray.length) {
                acc.push(charArray); //Only put non-empty arrays in our final list
            }
            return acc;
        }, []);
        console.log('possibilities >> ');
        possibilities.forEach(p => console.log(p));
        return possibilities;
    };

    var recurse = function (acc, arrays, idxArray, idx) {
        if (idx === arrays.length) {
            return acc.push(arrays.map((a, i) => a[idxArray[i]]))
        }

        for (let i = 0; i < arrays[idx].length; ++i) {
            idxArray[idx] = i;
            recurse(acc, arrays, idxArray, idx + 1);
        }
    };

    ns.handleChange = function (formValue) {
        //Clear any previous output
        var outputContainer = document.getElementById("output");
        outputContainer.innerHTML = '';

        //Build array values
        var combos = buildCombos(formValue);

        //Display output
        var str = "", div = document.createElement('pre'), frag = document.createDocumentFragment();
        combos.forEach(combo => {
            str += combo.join('');
            str += '\n';
        });
        div.textContent = str;
        frag.appendChild(div);
        outputContainer.appendChild(frag);
    };

    ns.buildCombos = buildCombos;  //Export for easier testing

})(PhoneNumberControls);